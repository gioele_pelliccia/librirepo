// Creare un'applicazione multipagina che sia in grado di gestire un elenco di libri.
// Ogni libro sarà caratterizzato da:
// - Titolo
// - Autore
// - Codice ISBN
// - Prezzo
// - Quantita

// Predisporre una pagina dedicata al solo inserimento
// Predisporre una pagina dedicata all'elencazione dei libri già inseriti, modifica ed eliminazione.

// HARD: Sulla tabella predisporre un campo di ricerca con un pulsante che come CTA filtri i risultati all'interno della tabella (per Titolo)
// HARD HARD: Alla fine della tabella immettere una riga con il TOTALE dei prezzi illustrati
// La tabella (alla ricerca) aggiorna il totale in tempo reale.
//MANDARE REPOSITORY SU ZULIP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


function inserisci(){
    let varTitolo = document.getElementById("inTitolo").value;
    let varAutore = document.getElementById("inAutore").value;
    let varCodice = document.getElementById("inCodice").value;
    let varPrezzo = document.getElementById("inPrezzo").value;
    let varQuantita = document.getElementById("inQuantita").value;

    let libro = {
        
        titolo: varTitolo,
        autore:varAutore,
        codice: varCodice,
        prezzo: parseFloat(varPrezzo),
        quantita:parseInt(varQuantita)
    }
    if(!isInElenco(libro)){

        elenco.push(libro);
        localStorage.setItem("listaLibri", JSON.stringify(elenco));
        alert("OK")
        document.getElementById("inTitolo").focus();
    }else{
        alert("Errore! Il codice coincide con quello un libro gia inserito")
        return;
    }
    document.getElementById("inTitolo").value="";
    document.getElementById("inAutore").value="";
    document.getElementById("inCodice").value="";
    document.getElementById("inPrezzo").value="";    
    document.getElementById("inQuantita").value="";
}
function isInElenco(varLibro){
    for(let [index,libro] of elenco.entries()){
        if(libro.codice==varLibro.codice){
            
            return true;
        }
    }
    return false;
}

function stampa(){
    let stringa ="";
    let counter = 0;
    for(let [index,libro] of elenco.entries()){
        counter+=libro.prezzo*libro.quantita;
        stringa+=`
            <tr class="riga">
                <td class="titolo">${libro.titolo}</td>               
                <td>${libro.autore}</td>               
                <td>${libro.codice}</td>               
                <td>${libro.prezzo}</td>               
                <td>${libro.quantita}</td>   
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="elimina(${libro.codice})">
                    <i class="fa-solid fa-trash"></i>
                    </button>

                    <button type="button" class="btn btn-outline-info" onclick="openModal(${libro.codice})">
                    <i class="fa-solid fa-pencil"></i>
                </td>         
            </tr>
            `;
    }
    document.getElementById("prezzocol").innerHTML=`<h3>Prezzo totale: </h3>`
    document.getElementById("contenuto").innerHTML= stringa;
    document.getElementById("prezzo").innerHTML=`<h3>${counter}</h3>`;
}
function elimina(varLibrocodice){
    for(let[index,item] of elenco.entries()){
        if(item.codice==varLibrocodice){
            elenco.splice(index,1);
            
        }
    }
    localStorage.setItem("listaLibri", JSON.stringify(elenco));
    stampa();
}
function openModal(varLibroCodice){
    for(let [index, item] of elenco.entries()){
        if(item.codice == varLibroCodice){
            document.getElementById("update_codice").value = item.codice;
            document.getElementById("update_titolo").value = item.titolo;
            document.getElementById("update_autore").value = item.autore;
            document.getElementById("update_prezzo").value = item.prezzo;
            document.getElementById("update_quan").value = item.quantita;
        }
    }

    $("#updateModale").modal('show');
}

function aggiorna(){
    let varTitolo = document.getElementById("update_titolo").value 
    let varAutore = document.getElementById("update_autore").value
    let varCodice = document.getElementById("update_codice").value
    let varPrezzo = document.getElementById("update_prezzo").value
    let varQuantita = document.getElementById("update_quan").value

    let libro = {
        
        titolo: varTitolo,
        autore:varAutore,
        codice: varCodice,
        prezzo: parseFloat(varPrezzo),
        quantita:parseInt(varQuantita)
    }
    for(let [index,item] of elenco.entries()){
        if(item.codice==libro.codice){
            item.titolo=libro.titolo;
            item.autore=libro.autore;
            item.prezzo=libro.prezzo;
            item.quantita=libro.quantita;
        }
    }
    $("#updateModale").modal('hide');
    stampa();
}

function ricerca(){
    let input = document.getElementById("inSearch").value;
    let counter = 0;
    
    if(input.length!=0){
        document.getElementById("contenuto").innerHTML= "";
        let stringa ="";       
        for(let libro of elenco){
            if(libro.titolo.toLowerCase().startsWith(input.toString().toLowerCase())){
                counter+=libro.prezzo*libro.quantita;
                stringa+=`
                    <tr >
                        <td >${libro.titolo}</td>               
                        <td>${libro.autore}</td>               
                        <td>${libro.codice}</td>               
                        <td>${libro.prezzo}</td>               
                        <td>${libro.quantita}</td>   
                        <td>
                            <button type="button" class="btn btn-outline-danger" onclick="elimina(${libro.codice})">
                            <i class="fa-solid fa-trash"></i>
                            </button>

                            <button type="button" class="btn btn-outline-info" onclick="openModal(${libro.codice})">
                            <i class="fa-solid fa-pencil"></i>
                        </td>         
                    </tr>
                    `;
            }        
        }
        document.getElementById("prezzocol").innerHTML=`<h3>Prezzo totale: </h3>`
        document.getElementById("prezzo").innerHTML=`<h3> ${counter} </h3>`;
        document.getElementById("contenuto").innerHTML= stringa;
        
    } else{
        stampa();
    }   
}  
if(localStorage.getItem("listaLibri") == null)
    localStorage.setItem("listaLibri", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("listaLibri"));

if(document.getElementById("contenuto") != null)
    stampa();


    